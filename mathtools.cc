/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

// By Yu Zhai <me@zhaiyusci.net> 

#include"mathtools.h"
double mel(const int N, const VectorXd& bra, const VectorXd& op, const VectorXd& ket){
  double res=0.0;
  res=0.0;
  for (int i =0; i!=N ;++i){
    res+=bra(i)*op(i)*ket(i);
  }
  return res;
}

