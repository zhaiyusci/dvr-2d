/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

// By Yu Zhai <me@zhaiyusci.net> 

#include<iostream>
#include<iomanip>
#include"podvr.h"

using namespace std;
using namespace Eigen;

extern"C"{
  void potread_(int*);
  void spl2_(double*, double*, double*);
}

const double amau=1822.887427;
const double hartreeincm=2*109737.32;
const double bohrinang=0.5291772108;
int main(){
  omp_set_num_threads(4);
  cout.setf(ios::fixed, ios::floatfield);
  cout << std::setprecision(12);
  int iv=0;
  potread_(&iv);
  VectorXd grid1(1);
  MatrixXd wf1(1,1);
  MatrixXd T1(1,1);
  VectorXd E1(1);
  VectorXd grid2(1);
  MatrixXd wf2(1,1);
  MatrixXd T2(1,1);
  VectorXd E2(1);
  VectorXd grid(1);
  MatrixXd wf(1,1);
  MatrixXd T(1,1);
  VectorXd E(1);
  const int PODVR_N1=11;
  const int PODVR_N2=11;
  const int pri_N1= 50;
  const int pri_N2= 50;
  double m1=8.0472572996*amau;
  double m2=3.2517473786*amau;
  extern double potential1(double);
  extern double potential2(double);
  extern double potential2d(double,double);

  sinc_podvr_1d(m1,pri_N1,PODVR_N1, -0.95/bohrinang, 1.0/bohrinang, grid1, E1, wf1, T1,potential1,true);
  cout << "grid1:"<<endl;
  cout << grid1<<endl;
  cout <<"E1:"<<endl;
  cout << E1*hartreeincm<<endl;
  cout << "wf1:"<<endl;
  cout<<wf1<<endl;
  sinc_podvr_1d(m2,pri_N2,PODVR_N2, -0.95/bohrinang, 1.0/bohrinang, grid2, E2, wf2, T2,potential2,true);
  cout << "grid2:"<<endl;
  cout << grid2<<endl;
  cout <<"E2:"<<endl;
  cout << E2*hartreeincm<<endl;
  cout << "wf2:"<<endl;
  cout<<wf2<<endl;
  MatrixXd H_PODVR(PODVR_N1*PODVR_N2, PODVR_N1*PODVR_N2);
  for(int i1=0; i1!=PODVR_N1; ++i1){
    for(int i2=0; i2!=PODVR_N2; ++i2){

      for(int j1=0; j1!=PODVR_N1; ++j1){
        for(int j2=0; j2!=PODVR_N2; ++j2){
          // Calculate <phi_i1|<psi_i2|H_1+H_2+V12|psi_j2>|phi_j1>
          // =H_1i1j1+H_2i2j2+pot2d()-pot1()-pot2()
          H_PODVR(i1*PODVR_N1+i2, j1*PODVR_N1+j2)=T1(i1,j1)*(i2==j2?1:0)+T2(i2,j2)*(i1==j1?1:0)
            +(( i1==j1 && i2==j2 )?  (potential2d(grid1(i1),grid2(i2))-potential1(grid1(i1))-potential2(grid2(i2))) :0.0)// Coupling term V12
            ;
        }
      }
    }
  }
  /*
   cout<< "T1"<<endl;
   cout<< T1<<endl;
   cout<< "T2"<<endl;
   cout<<T2<<endl;
   cout<< "H_PODVR"<<endl;
   cout<< H_PODVR<<endl;
   */

    SelfAdjointEigenSolver<MatrixXd> H_es(H_PODVR);
    E=H_es.eigenvalues();
    wf=H_es.eigenvectors();
    // H0_PODVR= podvrbasis.transpose()*T_energy*podvrbasis; // on PODVR basis


  cout<< "grid:"<<endl;
  grid= grid*bohrinang;
  for(int i1 =0; i1!=PODVR_N1;++i1) {
    for(int i2 =0; i2!=PODVR_N2;++i2) {
      cout << "     & "<< setw(20)<<i1<<"    "<<i2<<"    "<<grid1(i1)<<"d0,      "<< grid2(i2) <<endl;
    }
  }

  cout <<endl;
  cout <<endl;
  cout<< "E:"<<endl;
  E=E*hartreeincm;
  for(int i =0; i!=PODVR_N1*PODVR_N2;++i) cout << "     & "<< setw(20)<<E(i)<<"d0,"<<endl;

  cout <<endl;
  cout <<endl;

  cout << "wf:" <<endl;
  for(int i=0; i!=PODVR_N1*PODVR_N2; ++i){
    for(int j =0; j!=PODVR_N1*PODVR_N2;++j){
      cout << "     & "<< setw(20)<<wf(j,i)<<"d0,"<<endl;
    }
  }
  cout << "wf:" <<endl;
  cout << wf<<endl;
  cout << "fdjkfdsgh"<<endl;
  cout <<wf.col(0).transpose()*wf.col(1)<<endl;
  return 0;
}
/*
double potential(double x){
  double v;
  x*=bohrinang;
  spl1_(&x, &v);
  // v/=hartreeincm;
  return v;
}
*/

double potential2d(double x1, double x2){
  double v;
  x1*=bohrinang;
  x2*=bohrinang;
  spl2_(&x1,&x2,&v);
  return v;
}
  
double potential1(double x1){
  double x2=0.0;
  double y;
  y=potential2d(x1,x2);
  return y;
}

double potential2(double x2){
  double x1=0.0;
  double y;
  y=potential2d(x1,x2);
  return y;
}

