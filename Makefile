objects=sincdvr.o podvr.o mathtools.o 
CCFLAG= -Ieigen -fopenmp -Wall --std=c++11
FFLAG= -fopenmp -Wall
CXX=g++-4.9
F77=gfortran-4.9

all: testfa.x

test.x: test.o $(objects)
	$(CXX) -o $@ $(CCFLAG) test.o $(objects)

$(objects): %.o : %.cc %.h
	$(CXX) -o $@ -c $(CCFLAG) $< 

testfa.x: testfa.o 2d-pes.o $(objects) 
	$(CXX) -o $@ $(CCFLAG) testfa.o 2d-pes.o $(objects) -lgfortran

2d-pes.o: 2d-pes.f
	$(F77)  -c 2d-pes.f

testfa.o: testfa.cc podvr.h sincdvr.h mathtools.h
	$(CXX) -c -o $@ $(CCFLAG) testfa.cc 

.PHONY: clean all


clean:
	rm *.o *.x
