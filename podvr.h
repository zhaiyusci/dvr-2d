/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

// By Yu Zhai <me@zhaiyusci.net> 

#ifndef PODVR_H
#define PODVR_H
#include"mathtools.h"
#include"sincdvr.h"

using namespace Eigen;

extern int sinc_podvr_1d(double , int ,int , double , double , VectorXd&, VectorXd&, MatrixXd&, MatrixXd&, double (*)(double), bool);

#endif 
