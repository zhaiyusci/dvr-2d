c  "call potread" at the beginning of the code
c   call  pes(r,v) to calculate the potential
c   where, r is the bondlength of CO in angstrom,
c      implicit real*8(a-h,o-z)
c      open(1,file='outdata.dat')
c      call potread
c        rr=1.5 
c        call pes(rr,vx)
c       write(1,999)rr,vx
c999    format(1x,f6.3,f16.8)
c       end
 


      subroutine potread
      implicit real*8(a-h,o-z)
      parameter (nr=40)
C      data vmin/-139.74375689d0/
      data vmin/-189.58358925d0/
      common /pes/r(nr),ve(nr)
      open(98,file='normalpes.dat')
      !open(98,file='pes.dat')
      open(3,file='check-1.dat')
      rewind(98)
      read(98,*)
      do i=1,nr
c       k=nr-i+1
       k=i
      read(98,*)r(k),ve(k)
           ve(k)=ve(k)-vmin
           write(3,'(1x,f12.8,f14.6)')r(k),ve(k) 
      enddo 
      end

      subroutine spl1(ra,ysp)
      implicit real*8(a-h,o-z)
      parameter (nr=40)
      parameter (m=50,n=50,l=50)
      dimension xt(m)  
      dimension y(m),y2(l)
      common /pes/r(nr),ve(nr)
      data dy1,dyn/1.0d30,1.0d30/
      nr1=0
      do 2 i=1,nr
        nr1=nr1+1
        xt(nr1)=r(i)
        y(nr1)=ve(i)
 2    continue
      call spline(xt,y,nr1,dy1,dyn,y2)
      call splint(xt,y,y2,nr1,ra,y1)
 22   continue
      ysp=y1
      end


C##################################################################
C# SPLINE ROUTINES
C#            Numerical recipes in fortran
C#            Cambrige University Press
C#            York, 2nd edition, 1992.
C##################################################################
      SUBROUTINE splint(xa,ya,y2a,n,x,y)
      implicit double precision  (a-h,o-z)
      DIMENSION xa(n),y2a(n),ya(n)
      klo=1
      khi=n
 1    if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.0d0) write(6,*) 'bad xa input in splint'
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**
     *2)/6.0d0
      return
      END
C##############################################################################
      SUBROUTINE spline(x,y,n,yp1,ypn,y2)
      implicit double precision  (a-h,o-z)
      DIMENSION x(n),y(n),y2(n)
      PARAMETER (NMAX=100)
      DIMENSION u(NMAX)
      if (yp1.gt..99d30) then
        y2(1)=0.0d0
        u(1)=0.0d0
      else
        y2(1)=-0.5d0
        u(1)=(3.0d0/(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do 11 i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.0d0
        y2(i)=(sig-1.0d0)/p
        u(i)=(6.0d0*((y(i+1)-y(i))/(x(i+
     *1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*
     *u(i-1))/p
11    continue
      if (ypn.gt..99d30) then
        qn=0.0d0
        un=0.0d0
      else
        qn=0.5d0
        un=(3.0d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.0d0)
      do 12 k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
12    continue
      return
      END




