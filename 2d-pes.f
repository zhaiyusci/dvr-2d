c   ab inito potential energy surface for the vibratial averaged TD CO2-H2 
c   Please add the following statement in your main program:
c   call potread
c   use the following statement to calculate the PES when needed:
c   call ocshepes(rr1,theta,v)
c   rr1 is R in angstrom,
c   theta is the enclosed angle in degree
c   v is the potential in cm-1 

c      implicit real*8(a-h,o-z)
c      open(7,file='3-6pes.out')
c      do rr2=-1.0d0,1.0001d0,0.02d0
c      do rr1=-1.0d0,1.0001d0,0.02d0
c           call mgh2pot(v,rr1,rr2,0)    
c           write(7,999)rr2,rr1,v
c      enddo;enddo    
c 999  format(1x,2f10.4,f15.8)  
c      end

      subroutine potread(iv)
      implicit real*8(a-h,o-z)
      parameter (pi=3.141592653589793d0)    
      parameter (nr1=41,nth=41)
      dimension vg(nr1,nth),ve(nr1,nth)
      common /pes/pesmin,ri(nr1),thek(nth),vge(nr1,nth)
c      data vming/-50.735794d0/
c      data vmine/-50.583631d0/
      data vasy/0.0d0/
      open(98,file='3-6pes.dat')
      open(99,file='check')
c     energy cut by 5.0 ev
      vcut=5./27.212d0
c      read(98,*)
c      read(98,*)
      pesmin=10000
      do 10 k=1,nth
        do 10 i=1,nr1
        read(98,*)thek(k),ri(i),vg(i,k)
 10   continue
 11   continue
      do 12 k=1,nth 
      do 12 i=1,nr1 
       if(iv.eq.0)then
         vge(i,k)=vg(i,k)
         else
         vge(i,k)=ve(i,k)  
        endif
        if (vge(i,k).lt.pesmin) then
          pesmin=vge(i,k)
          r1m=ri(i)
          thetam=thek(k)
         endif
c         if (vge(i,k).gt.vcut) vge(i,k)=vcut
  12   continue
        write(*,*)'r1m= ',r1m,'R= ',thetam,'vmin=',pesmin
       do k=1,nth
       do i=1,nr1
          vge(i,k)=vge(i,k)-pesmin
          if(vge(i,k).gt.vcut)vge(i,k)=vcut
          write(99,1001)thek(k),ri(i),vge(i,k)
 1001  format (1x,2f10.4,f15.7)
       enddo;enddo
       end

      subroutine mgh2pot(XV,XR1,XR2,iv)
      implicit real*8(a-h,o-z)
      parameter (nr1=41,nth=41)    
      parameter (rbohr=0.5291772108d0)
      parameter (pi=3.141592653589793d0)
      common /pes/pesmin,ri(nr1),thek(nth),vge(nr1,nth)
c-----------------------------------------------------------------------
      DATA IPAR/0/
      SAVE IPAR
c-----------------------------------------------------------------------------
      IF(IPAR.EQ.0) THEN
          CALL potread(iv)
           IPAR= 1
           ENDIF
      rr1=XR1
      rr2=XR2
      if (rr1.lt.-2.0d0) rr1=-2.0d0
      if (rr1.gt.2.0d0) rr1=2.0d0
      if (rr2.lt.-2.0d0) rr2=-2.0d0
      if (rr2.gt.2.0d0) rr2=2.0d0
         call spl2(rr1,rr2,vt)
c        write(11,*)rr1,thth,vt
      XV=vt
      end

      subroutine spl2(r10,th,vt)
      implicit real*8(a-h,o-z)
      parameter (nr1=41,nth=41)    
      parameter (m=100,n=100,l=100,ir2=0)
      dimension xt(m)  
      dimension dty(l),ddty(l),s1(1),ds1(1),dds1(1),h1(l)
      dimension dny(n),ddny(n),s2(1),ds2(1),dds2(1),h2(n)
      dimension dhy(m),ddhy(m),s3(1),ds3(1),dds3(1),h3(m)
      dimension y(m),ss(m),sss(m),y2(l)
      common /pes/pesmin,ri(nr1),thek(nth),vge(nr1,nth)
      data dy1,dyn/1.0d30,1.0d30/
      do 10 i=1,nr1
        nh=0
        do 2 k=1,nth
          nh=nh+1
          xt(nh)=thek(k)
          y(nh)=vge(i,k)
   2  continue
c	if (th.lt.xt(1)) th=xt(1)
      call spline(xt,y,nh,dy1,dyn,y2)
      call splint(xt,y,y2,nh,th,y3)
  22  continue
        ss(i)=y3
  10  continue
      nr=0
      do 5 i=1,nr1
        nr=nr+1
        xt(nr)=ri(i)
        y(nr)=ss(i)
   5  continue
      call spline(xt,y,nr,dy1,dyn,y2)
      call splint(xt,y,y2,nr,r10,yw2)
 33   continue
        vt=yw2
      end


C##################################################################
C# SPLINE ROUTINES
C#            Numerical recipes in fortran
C#            Cambrige University Press
C#            York, 2nd edition, 1992.
C##################################################################
      SUBROUTINE splint(xa,ya,y2a,n,x,y)
      implicit double precision  (a-h,o-z)
      DIMENSION xa(n),y2a(n),ya(n)
      klo=1
      khi=n
 1    if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x)then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.0d0) write(6,*) 'bad xa input in splint'
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      y=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**
     *2)/6.0d0
      return
      END
C##############################################################################
      SUBROUTINE spline(x,y,n,yp1,ypn,y2)
      implicit double precision  (a-h,o-z)
      DIMENSION x(n),y(n),y2(n)
      PARAMETER (NMAX=100)
      DIMENSION u(NMAX)
      if (yp1.gt..99d30) then
        y2(1)=0.0d0
        u(1)=0.0d0
      else
        y2(1)=-0.5d0
        u(1)=(3.0d0/(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do 11 i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.0d0
        y2(i)=(sig-1.0d0)/p
        u(i)=(6.0d0*((y(i+1)-y(i))/(x(i+
     *1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*
     *u(i-1))/p
11    continue
      if (ypn.gt..99d30) then
        qn=0.0d0
        un=0.0d0
      else
        qn=0.5d0
        un=(3.0d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.0d0)
      do 12 k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
12    continue
      return
      END

