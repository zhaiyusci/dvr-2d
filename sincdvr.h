/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

// By Yu Zhai <me@zhaiyusci.net> 

#ifndef DVR_H
#define DVR_H
#include"mathtools.h"

using namespace Eigen;

extern int sinc_dvr_1d(double , int , double , double , VectorXd&, VectorXd&, MatrixXd&, MatrixXd&, double (*)(double));
// extern double potential(double);

#endif 
